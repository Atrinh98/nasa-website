# nasa website

## Demo

https://www.youtube.com/watch?v=lSMccVf845E

## Members

- Andrew Trinh
- Hendrik Tebeng

## Description

A simple website where you get the price of stocks.

## Technology Stack

- JavaScript
- HTML
- CSS
- NASA API

## How to Run

Got to the following link: https://nasa-retrieval-website.onrender.com

## Usage

Once on the website, you will be presented with the home page that has a text field and a big button.

You can:
- Start pressing on the Retrieve NASA Information.
    - Information will show.
- You can have up to 5 buttons of information queued up.
- When pressing on one of the information button.
    - It will bring you back to that previous information.

## Credits

https://api.nasa.gov/